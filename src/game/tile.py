import pygame


class Tile:
    # Tile for the game board
    def __init__(self, rect: pygame.Rect):
        self.rect = rect
        self.player_num = 0
        self.starting_point = False
        self.round = -1
