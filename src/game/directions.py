from enum import Enum


class Direction(Enum):
    up = 'up'
    left = 'left'
    down = 'down'
    right = 'right'

    def toLeftTurn(self) -> 'Direction':
        # turns a direction left
        if self == Direction.up:
            return Direction.left
        if self == Direction.left:
            return Direction.down
        if self == Direction.down:
            return Direction.right
        if self == Direction.right:
            return Direction.up

    def toRightTurn(self) -> 'Direction':
        # turns a direction right
        if self == Direction.up:
            return Direction.right
        if self == Direction.left:
            return Direction.up
        if self == Direction.down:
            return Direction.left
        if self == Direction.right:
            return Direction.down
